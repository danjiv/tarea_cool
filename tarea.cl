--Daniel Alberto Jiménez Vargas

class Main inherits IO {
    main(): SELF_TYPE {
        {

            let number: Int <- 0,
        	number1: Int<-0,
        	total:Int<-0
        in {
            out_string("Please enter your number a:\n");
            number <- in_int();
            out_string("Please enter your number b:\n");
            number1 <- in_int();

            total<-mcd(number,number1);
            out_string("El mcd es: ");
            out_int(total);
            out_string("\n");
            };
          }
     };

    mcd(x:Int, y:Int):Int{
        if(y=0)
        	then x
        else mcd(y,mod(x,y))
        fi
    };

    mod(a:Int, b:Int):Int{
        if((a-b)<0)
        	then a
        else mod(a-b,b)
        fi
	};
};
